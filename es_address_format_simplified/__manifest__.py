# -*- coding: utf-8 -*-
{
    'name': "es_address_format_simplified",

    'summary': """
        Address format for Spain with no country and address in one line
    """,
    'description': """
        Address format for Spain with no country and address in one line
    """,

    'author': "Coopdevs SCCL",
    'website': "http://www.coopdevs.org",

    'category': 'Localization',
    'version': '12.0.0.0.3',

    'depends': ['base'],

    'data': [
        'data/res_country_data.xml',
    ],
    'installable': True,
    "license": "AGPL-3",
}
